import numpy as np

class RootNotFoundError(Exception):
   """Raised when root not found"""
   pass


def solveByBisect(f, a, b, nmax=100, e=1e-6):
    "Solve function f by bisection method."
    "Solve to error e starting from a and b. Maximum nmax iterations."

    # Error checking
    if nmax <= 0:
        raise ValueError('Argument nmax to solveByBisect should be >0')
    if e <= 0:
        raise ValueError('Argument e to solveByBisect should be >0')
    if not(isinstance(a, float)) or not(isinstance(b, float)):
        raise TypeError('Arguments a and b to solveByBisect should be a floats')
    if not(callable(f)):
        raise Exception('A callable function must be sent to solveByBisect')

    # Iterate until the solution is within the error or too many iterations
    for it in range(nmax):
        c = 0.5*(a+b)
        if f(a)*f(c) < 0:
            b=c
        else:
            a=c
            if abs(f(c)) < abs(e):
                break # Break out of the for loop
    else:             # If you haven’t broken out of the for loop by the end,
        raise RootNotFoundError("No root found by solveByBisect") # raise an exception

    return c, it+1

# Making assertions
assert abs(np.cos(solveByBisect(np.cos, 0.0, np.pi)[0])) < 1e-6, \
"solveByBisect gave the wrong answer for cos"
assert abs(solveByBisect(lambda x: 2*x+3, -10.0,10.0)[0] + 1.5) < 1e-6, \
"solveByBisect gave the wrong answer for a linear function"

try:
    solveByBisect(np.cos, -1., 1.)
except RootNotFoundError:
    pass
else:
    print("Error in solveByBisect, an exception should be raised \
    as there is no root of cos between -1. and 1.")

try:
    solveByBisect(0,1.,3.)
except:
    pass
else:
    print("Error in solveByBisect, an exception should be raised if no function is passed.")

try:
    solveByBisect(np.cos, 1.,1.,0)
except ValueError:
    pass
else:
    print("Error in solveByBisect, an exception should be raised if nmax <= 0")

try:
    solveByBisect(np.cos, 1.,1.,1,0)
except ValueError:
    pass
else:
    print("Error in solveByBisect, an exception should be raised if e <= 0")
